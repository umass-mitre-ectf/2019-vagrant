# ZOO_MES Secure Entertainment System
### 2019 MITRE Collegiate eCTF

This repository contains the University of Massachusetts Amherst a/k/a ZOO_MES MITRE Entertainment System source code.

This system was based off of [the supplied sample implementation](https://github.com/mitre-cyber-academy/2019-ectf-insecure-example).

## Important Note

Before running `vagrant up`, set the variable `$git_key_path` in
`provision/config.rb:46` to the path of the SSH key you wish to use for our
GitLab-hosted repository.

With this done, the key will be copied to the VM and added to the SSH agent.
Then, the repository will be cloned following normal procedure.
